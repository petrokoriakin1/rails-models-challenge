# frozen_string_literal: true

class Order < ApplicationRecord
  belongs_to :user
  belongs_to :owning, polymorphic: true

  has_many :order_items, dependent: :destroy
end
