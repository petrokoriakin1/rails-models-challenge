# frozen_string_literal: true

class Item < ApplicationRecord
  belongs_to :restaurant
  belongs_to :restaurant_category

  has_many :order_items, as: :orderable, dependent: nil
end
