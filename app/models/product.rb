# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :shop
  belongs_to :category

  has_many :order_items, as: :orderable, dependent: nil
end
