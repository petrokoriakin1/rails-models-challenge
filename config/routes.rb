# frozen_string_literal: true

Rails.application.routes.draw do
  get 'orders/create'
  get 'orders/show'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
