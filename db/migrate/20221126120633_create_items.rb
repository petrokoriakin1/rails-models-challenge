# frozen_string_literal: true

class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.integer :sort_order
      t.string :name
      t.references :restaurant, null: false, foreign_key: true
      t.references :restaurant_category, null: false, foreign_key: true

      t.timestamps
    end
  end
end
