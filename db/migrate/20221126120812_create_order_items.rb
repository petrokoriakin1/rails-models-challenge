# frozen_string_literal: true

class CreateOrderItems < ActiveRecord::Migration[7.0]
  def change
    create_table :order_items do |t|
      t.integer :quantity
      t.references :order, null: false, foreign_key: true
      t.references :orderable, polymorphic: true, null: false
      t.text :note

      t.timestamps
    end
  end
end
