# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrdersController do
  describe '#create' do
    subject(:order) { create(:order, :with_products_and_items) }

    context 'when things are present and valid' do
      it 'persists the order' do
        expect(order).to be_persisted
      end

      it 'contains order order_items' do
        expect(order.order_items.count).not_to be_zero
      end

      it 'contains items' do
        expect(order.order_items.pluck(:orderable_type)).to include('Item')
      end

      it 'contains products' do
        expect(order.order_items.pluck(:orderable_type)).to include('Product')
      end
    end
  end
end
