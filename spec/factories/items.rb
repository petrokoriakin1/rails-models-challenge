# frozen_string_literal: true

FactoryBot.define do
  factory :item do
    sort_order { 1 }
    name { 'Pizza' }
    restaurant_category

    after(:build) do |item|
      item.restaurant = item.restaurant_category.restaurant
    end
  end
end
