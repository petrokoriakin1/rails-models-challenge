# frozen_string_literal: true

FactoryBot.define do
  factory :restaurant do
    name { 'Dominos Pizza' }
  end
end
