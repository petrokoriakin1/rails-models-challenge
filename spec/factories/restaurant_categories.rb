# frozen_string_literal: true

FactoryBot.define do
  factory :restaurant_category do
    name { 'Delicious' }
    restaurant
  end
end
