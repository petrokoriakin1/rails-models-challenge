# frozen_string_literal: true

FactoryBot.define do
  factory :order_item do
    quantity { rand(10) }
    note { 'A long long user request' }
  end

  trait :product do
    after(:build) do |order_item|
      order_item.orderable = FactoryBot.build(:product)
    end
  end

  trait :item do
    after(:build) do |order_item|
      order_item.orderable = FactoryBot.build(:item)
    end
  end
end
