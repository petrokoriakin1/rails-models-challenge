# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { 'John' }
    last_name { 'Testenko' }
    email { 'testenko@example.com' }
  end
end
