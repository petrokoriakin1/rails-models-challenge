# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    name { 'Banana' }
    shop
    category
  end
end
