# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    user

    after(:build) { |order| order.owning = order.user }
  end

  trait :with_products_and_items do
    after(:build) do |order|
      order.order_items << FactoryBot.build(:order_item, :product)
      order.order_items << FactoryBot.build(:order_item, :item)
    end
  end
end
