
# Problem statement

## Given the following models:

```
Restaurant
  id
  name

RestaurantCategory  (ie: drinks, burgers)
  id
  name
  restaurant_id

Item           # for restaurants
  name
  restaurant_id
  restaurant_category_id
  sort_order (in restaurant_category_id)

Product        # for shops
  id
  name
  shop_id
  category_id


Order
  id
  user_id
  owning_id
  owning_type

OrderItem
  id
  order_id
  orderable_id
  orderable_type
  quantity
  note
```
## Assignments

- Implement the models in code. 
  - Keep in mind the following questions:
    - what are some model scopes/relationships you would create?
    - Which classes you’d place them in, and why?
    - Are you repeating yourself or making your life easier?
    - Is syntactic sugar good or bad?
- How do you feel about having restaurant_id under item? Is it necessary when we have restaurant_category_id? Would you keep it or remove it and why?
- Why do we have the below? What do we call such a relationship?
```
owning_id     (ownable)
owning_type
orderable_id  (orderable)
orderable_type
```
- When are the relationships in 3 bad? ie: when should we use flat tables instead of joining tables? Think about scalability.
